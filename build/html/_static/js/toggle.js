jQuery(document).ready(function(){

  var checkbox = $('input[name=mode]');
  var initial = checkbox.prop('checked');
  var mode = localStorage.getItem('dark_mode');

  if (mode != null){
    isTrue = (mode === "true");
    $('input[name=mode]').prop('checked', isTrue);
    toggleCssMode(isTrue);
  } else {
    toggleCssMode(initial);
  }
  checkbox.click(function() {
    toggleCssMode(this.checked);
  })
});

function toggleCssMode(mode) {
  localStorage.setItem('dark_mode', mode);
  var url = document.URL;
  var href = "../../_static/css/dark.css"

  if (/TP/.test(url)) {
    href = "../../" + href
  }
  if (/html\/index/.test(url)) {
    href = "_static/css/dark.css"
  }
  if (mode == true) {
      $('head').append('<link rel="stylesheet" id="dark-mode" href="' + href + '" type="text/css" />');
  } else {
    $('link#dark-mode').remove();
  }
}
