.. |br| raw:: html

  <br />

.. role:: underline


****************************
BDD : Effectuer ses requêtes
****************************


Drupal possède sa propre API pour effectuer des requêtes en base de données.
Cette couche d'abstraction est construite sur l'extension PHP PDO et hérite en
grande partie de sa syntaxe et de sa sémantique.

Dans cette partie, nous allons voir deux aspects du système de gestion de base
de données, inhérentes à la manière dont Drupal gère ses données avec :

* les requêtes faites sur les tables ;
* les requêtes faites sur les entités ;


Requête sur les tables
======================

Pour effectuer des requêtes directement sur les tables, nous allons utiliser le
service ``database``. Nous verrons dans cette partie les grandes étapes à respecter,
à savoir,

* Initialiser la connexion ;
* Construire sa requête ;
* Consulter les résultats ;

Si vous souhaitez aller plus loin dans l'appréhension de ces concepts, vous pouvez
consulter `la Database API`_.

.. _la Database API: https://www.drupal.org/docs/8/api/database-api

Initialiser la connexion
-------------------------

C'est un service ... Donc deux manières de procéder, fonction que l'on travaille
dans une classe ou dans un script.

.. tabs::

  .. tab:: Dans une classe

    **Ex. Controller, formulaire, block ...** |br|
    Toujours favoriser l'injection de dépendances.

    .. code-block:: php

      /**
       * @var \Drupal\Core\Database\Driver\mysql\Connection
       */
      protected $database;

      public function __construct(Connection $database) {
        $this->database = $database;
      }
      public static function create(ContainerInterface $container) {
        return new static(
          $container->get('database')
        );
      }
      public function render(){
        $query = $this->database->select('[table]', '[alias]');
      }

  .. tab:: Dans un script

    **Ex. hook, preprocess...** |br|

    .. code-block:: php

      $connection = \Drupal::database();
      $connection = \Drupal::service('database'); // équivalent
      $query = $connection->select('[table]', '[alias]');

Construire sa requête
---------------------

Vous pouvez construire toutes les requêtes usuels, à savoir,

* `select`_ ;
* `update`_ ;
* `insert`_ ;
* `delete`_ ;

.. _select: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/introduction-to-dynamic-queries
.. _update: https://www.drupal.org/docs/8/api/database-api/update-queries
.. _insert: https://www.drupal.org/docs/8/api/database-api/insert-queries
.. _delete: https://www.drupal.org/docs/8/api/database-api/delete-queries

Chaque type de requête ayant sa propre syntaxe, je vous invite à consulter plus
en détails la documentation pour chacune d'entre elle.

.. code-block:: php
  :caption: Un exemple de select

  $connection = \Drupal::database();
  $query = $this->database->select('users', 'u')
    ->fields('u', ['uid', 'uuid'])
    ->condition('u.uid' , 1);
  $result = $query->execute();

Consulter les résultats
------------------------

Deux manière de traiter le résultat de cette requête :

  **En parcourant de manière itérative l'ensemble des résultats**

  .. code-block:: php

    foreach ($result as $record) {
      dump($record);
    }


  **En agrégeant les résultats**

  .. code-block:: php

    $result->fetchField()         // la valeur du premier field, du premier resultat ;
    $result->fetchAll()           // Un tableau contenant tous les résultats, avec tous les fields retournés ;
    $result->fetchCol()           // Un tableau contenant tous les résultats, mais seulement la valeur du premier field ;
    $result->fetchAssoc()         // Un tableau associatif du premier résultat ;
    $result->fetchAllAssoc($key)  // Un tableau associatif de tous les résultats, mappé sur la clé passé en paramètre ;
    $result->fetchAllKeyed()      // Les résultats sous forme de tableau indexé par la 1ere colonne avec pour valeur la 2e ;

  `Pour en savoir plus`_

.. _Pour en savoir plus: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Statement.php/class/Statement/8.8.x


Pour faire une requête sur les entités
======================================

Les requêtes sur les entités se font à l'aide de l'``entityTypeManager``. Il est possible
de faire des requêtes sur :

* Les nœuds ;
* Les blocs ;
* Les utilisateurs ;
* Les menus ;
* Les éléments de menus ;
* Nos types d'entités custom ;

On réalise les conditions sur les champs qui configurent notre entité. La requête
nous renvoie un tableau de nids répondant à nos conditions.

:underline:`Deux articles sur le sujet :`

* `The Drupal 8 version of EntityFieldQuery`_ ;
* `Les EntityQuery par l'exemple`_ ;

.. _The Drupal 8 version of EntityFieldQuery: https://www.sitepoint.com/drupal-8-version-entityfieldquery/
.. _Les EntityQuery par l'exemple: https://kgaut.net/blog/2017/drupal-8-les-entityquery-par-lexemple.html

:underline:`Documentation :`

* `QueryInterface`_

.. _QueryInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition/8.8.x


Initialiser la connexion
-------------------------

Comme précédemment, on va utiliser un service à utiliser tel que dans un script
ou à injecter dans une classe.

.. admonition:: À retenir
  :class: hint

  Dans un Controller, l'``entityTypeManager`` est déjà disponible car fourni par le
  la classe ``ControllerBase``

  .. code-block:: php

    $em = $this->entityTypeManager();


.. tabs::

  .. tab:: Dans un composants

    .. code-block:: php

      /**
       * @var \Drupal\Core\Entity\EntityTypeManager
       */
      protected $entityTypeManager;

      public function __construct(EntityTypeManager $entity_type_manager) {
        $this->entityTypeManager = $entity_type_manager;
      }
      public static function create(ContainerInterface $container) {
        return new static(
          $container->get('entity_type.manager')
        ) ;
      }
      public function build(){
        $entity_ids = $this->entityTypeManager->getStorage('node')->getQuery()
          ->execute();
        $result = sizeof($entity_ids);
      }

  .. tab:: Dans un script

    .. code-block:: php

      $entity_ids  = \Drupal::entityQuery('node')
        ->execute();
      $result = sizeof($entity_ids);


Construire sa requête
---------------------

À la différence des requêtes faites sur les tables - où vous aviez la possibilité
d'effectuer toutes les requêtes courantes - vous ne pouvez que faire des requêtes
de type ``select``, en posant les conditions sur les champs de l'entité. Pour
modifier ou supprimer une entité, il faudra la charger, en modifier les valeurs
puis l'enregistrer.

.. code-block:: php

  $entity_ids = $this->entityTypeManager->getStorage('node')->getQuery()
    ->condition('type', 'bundle_type', '')
    ->condition('field_date', '2019-09-19')
    ->execute();
  $result = sizeof($entity_ids);


Parcourir les résultats
-----------------------

La requête renvoie un tableau d'identifiant correspondant aux entités recherchées.
Vous pouvez ensuite parcourir ce résultats dans une structure itérative.

.. NOTE::
  Question performance, préférer charger toutes les entités d'une seul coup avec
  la fonction ``loadMultiple()`` plutôt que d'effectuer plusieurs ``load()``

.. code-block:: php

  $entities = $this->entityTypeManager->getStorage('node')->loadMultiple($entity_ids)
  foreach ($entities as $entity) {
    dump($entity);
  }
