.. |br| raw:: html

  <br />


****************
Pour vous guider
****************


D'une manière générale
======================

* Vous pouvez utiliser un ``dump()`` pour inspecter la structure des données ;
* Vous avez le droit de faire des recherches sur internet 🔎 ;


Pour le niveau 1
================

* Pensez à utiliser les fonctions mises à disposition par le ``ControllerBase`` ;
* Le thème *liste* existe déjà ! Jetez un petit coup d'oeil aux `thèmes communs`_
  implémentés nativement ;
* Utilisez les propriétés du `render array`_ ;

.. _thèmes communs: https://api.drupal.org/api/drupal/core%21includes%21theme.inc/function/drupal_common_theme/9.x
.. _render array: https://www.drupal.org/docs/8/api/render-api/render-arrays


Pour le niveau 2
================

* Pour définir l’ancienneté d'un utilisateur, vous pouvez utiliser le service
  `date.formatter`_ ;
* Le thème *tableau* existe déjà !
* Pour gérer le rendu de la liste dans le tableau, vous pourrez vous aider du
  service `renderer`_ ;
* C'est dans la configuration de votre route qui vous gérerez l'accès au controller ;

.. _date.formatter: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Datetime%21DateFormatter.php/class/DateFormatter/9.x
.. _renderer: https://api.drupal.org/api/drupal/core!core.services.yml/service/renderer/9.x


Pour le niveau 3
================

* Regarder de plus près `les possibilités de configurations`_ d'une route. Je ne vous en dis pas plus, c'est le niveau 3 !

.. _les possibilités de configurations: https://www.drupal.org/node/2092643
