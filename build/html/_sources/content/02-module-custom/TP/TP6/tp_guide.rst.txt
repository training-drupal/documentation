.. # define break line
.. |br| raw:: html

  <br />

****************
Pour vous guider
****************

Pour le niveau 1
================

* Il faudra injecter le service qui nous permettra de faire des requête dans
  la base de données ;
* Vous aurez deux requêtes à construire :

  * Une requête de type select pour vérifier la pré-existance de
    l'adresse mail ;
  * Une requête de type insert pour compléter votre base de données ;

* Pour la question subsidiaire, je vous renvoie à la
  `documentation <https://www.drupal.org/docs/8/api/update-api/updating-database-schema-andor-data-in-drupal-8>`_


Pour le niveau 2
================

* Il faudra utiliser les requêtes sur les entités, et particulièrement
  `la QueryAggregateInterface <https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryAggregateInterface.php/interface/QueryAggregateInterface/8.8.x>`_
* Pour vous guider sur la question subsidiaire :

  * Si vous avez utilisé un Controller, plusieurs solutions possibles :

      * Construire une route qui accepte un paramètre ;
      * Utiliser un formulaire intermédiaire et renvoyer vers un Controller qui
        traite le résultat ;

  * ... Voire mieux, `un formulaire Ajax <https://www.drupal.org/docs/8/api/javascript-api/ajax-forms>`_ ;

  * Si vous avez utilisé un Block, plusieurs solutions possibles (fonction de
    la configuration pour l'affichage du bloc) :

      * Utiliser un formulaire de configuration ;
      * Récupérer dynamiquement, à partir de la route, l'information relative
        au sport


Pour le niveau 3
================

La requête n'est pas très compliqué à construire, la difficulté de l'exercice
réside plus dans un point que l'on a peut abordé, la gestion des assets...
Et bien sûr l'implémentation dans un hook_theme() ;
Pas de panique, la librairie est très facile à utiliser !!
