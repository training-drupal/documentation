.. |br| raw:: html

  <br />

.. role:: red
.. role:: underline


*************************
Les outils du développeur
*************************


Les indispensables
==================

:underline:`Un bon terminal`

  Pour Linux, `Tilix`_ ; |br|
  pour Windows, `Cmder`_, un émulateur de ligne de commande Linux ;

.. _Tilix: https://gnunn1.github.io/tilix-web/
.. _Cmder: https://cmder.net/

:underline:`Un bon IDE avec un debugger intégré (Xdebug)`

  Atom, phpStorm, VisualStudio, Eclipse ;

:underline:`Quelques plugins utiles pour votre navigateur`

    Firefox Multi-Account Containers ; |br|
    OpenLorem ; |br|
    Awesome Timestamp ; |br|
    Xdebug-ext ;


Le serveur Web
==============

Télécharger un utilitaire de gestion de serveurs pour installer en local votre site :

* Windows : `WAMPP`_ ;
* Linux : `XAMMP`_ ;
* Mac : `MAMP`_ ;

.. _WAMPP: https://www.wampserver.com/
.. _XAMMP: https://www.apachefriends.org/index.html
.. _MAMP: https://www.mamp.info/en/mac/

.. NOTE::
  * Les distributions Linux sont déjà dotées d'un serveur Apache et Mysql - MariaDB ;
  * Pensez éventuellement à des solutions de conteneurisation - *Docker, VM* -
    si vous avez plusieurs projets sur votre machine nécessitant des configurations
    différentes ;


Git
===

`Git`_ est essentiel tout au long de votre projet, pour :

* Partager votre code ;
* Versionner vos étapes de développement ;
* Organiser vos livraisons ;
* Faciliter le déploiement / la synchronisation de votre application ;

.. _Git: https://git-scm.com/

!!!!

.. rst-class:: uppercase-text

Installation

.. tabs::

  .. tab:: Linux

    ::

      sudo apt-get install git-all

  .. tab:: Windows

    `Télécharger l’exécutable`_

.. _Télécharger l’exécutable: http://git-scm.com/download/win

Composer
========

`Composer`_ est un utilitaire php qui permet de déclarer et d'installer des librairies
nécessaires à un projet. Outre le versionning, Composer permet également de résoudre
les dépendances entre les librairies.

Par défaut, Composer va rechercher le core de Drupal dans le dépôt officiel,
mais pour l'ensemble des modules communautaires, Composer recherche dans un dépôt
spécifique, déclaré dans le ``composer.json``.

.. _Composer: https://getcomposer.org

.. code-block:: bash

  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    }
  ],


Tout au long de votre projet, Drupal & les modules communautaires seront gérés
avec Composer pour leur installation et leur mise à jour.

!!!!

.. rst-class:: uppercase-text

Installation

.. tabs::

  .. tab:: Linux

    .. code-block:: console

      php -r "copy('https://getcomp  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
      php composer-setup.php
      php -r "unlink('composer-setup.php');"
      # Pour installer Composer globalement sur votre machine
      mv composer.phar /usr/local/bin/composer
      # Pour connaître les fonctions utilisables
      composer

  .. tab:: Windows

    * Télécharger l’exécutable : `https://getcomposer.org/download/`_ ;
    * **OU** utiliser la procédure Linux pour l'utiliser dans l'émulateur de ligne
      de commande **cmder**. On s’arrêtera au téléchargement du fichier ``composer.phar``.

.. _https://getcomposer.org/download/: https://getcomposer.org/download/


Drush
=====

Drush = Drupal Shell |br|
C'est un ensemble de scripts qui facilitent l'administration de son site Drupal
au quotidien.

* Pour vider les caches ;
* Pour installer / désinstaller des modules ;
* Pour gérer l'import/export des configurations ;
* Pour installer sur site ;
* Pour mettre à jour, exporter sa bdd ;
* ... ;

.. admonition:: Documentation
  :class: Note

  `Liste des commandes drush`_

.. _Liste des commandes drush: https://drushcommands.com

!!!!

.. rst-class:: uppercase-text

Installation

.. tabs::

  .. tab:: Dans le projet

    Installer Drush avec Composer, au lancement de votre projet.

    .. parsed-literal::

      # Root of the Drupal project
      composer require drush/drush

      # Accéder à l'ensemble des commandes
      vendor/bin/drush

  .. tab:: Launcher Linux

    Pour exécuter Drush dans n'importe quelle projet Drupal.

    * `http://docs.drush.org/en/master/install/ <http://docs.drush.org/en/master/install/>`_
    * `https://github.com/drush-ops/drush-launcher/ <https://github.com/drush-ops/drush-launcher/>`_

    .. code-block:: console

      wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar
      chmod +x drush.phar
      sudo mv drush.phar /usr/local/bin/drush
      drush

  .. tab:: Launcher Windows

    Pour exécuter Drush dans n'importe quelle projet Drupal.

    * `https://github.com/drush-ops/drush-launcher <https://github.com/drush-ops/drush-launcher>`_
    * `http://modulesunraveled.com/drush/installing-drush-windows <http://modulesunraveled.com/drush/installing-drush-windows>`_


La console Drupal
=================

`La console Drupal`_ est un autre outil ligne de commande, adapté de la console
Symfony, pour interagir avec votre système. En complément de Drush - *plutôt dédié
à la gestion courante de votre site* - la console Drupal est un outil d'aide au
développement, principalement utile pour générer du code "standardisé", autrement dit
des squelettes de composants : module, theme, controller, block ...

Vous trouverez `dans cet article`_ de plus amples explications sur la différence
entre ces deux outils.

.. _La console Drupal: https://drupalconsole.com/docs/en/
.. _dans cet article: https://pantheon.io/drupal-8/introduction-drush-and-drupal-console

!!!!

.. rst-class:: uppercase-text

Installation


.. tabs::

  .. tab:: Dans le projet

    Installer la console avec Composer, au lancement de votre projet.

    .. code-block:: bash

      # Root of the Drupal project
      composer require drupal/console:~1.0 \
      --prefer-dist \
      --optimize-autoloader

      # Accéder à l'ensemble des commandes
      vendor/bin/drupal


    .. NOTE::
      Vous pouvez vous retrouver avec des conflits de librairies, qu'il faudra résoudre
      de la manière suivante :

      * Supprimer le ``composer.lock`` et le dossier ``vendor`` ;
      * Rajouter manuellement dans le ``composer.json``, la ligne ``"drupal/console": "~1.0"`` ;
      * Lancer l'installation du projet avec ``composer install``


  .. tab:: Launcher Linux

    Pour exécuter la console dans n'importe quelle projet Drupal.

    * `http://docs.drush.org/en/master/install/ <http://docs.drush.org/en/master/install/>`_
    * `https://github.com/drush-ops/drush-launcher/ <https://github.com/drush-ops/drush-launcher/>`_

    .. code-block:: console

      php -r "readfile('https://drupalconsole.com/installer');" > drupal.phar
      mv drupal.phar /usr/local/bin/drupal
      chmod +x /usr/local/bin/drupal


CodeSniffer
===========

`Php Code Sniffer`_ est une librairie
qui permet de détecter les erreurs de style par rapport à un/des standard(s)
implémenté(s).

L'installation proposé ici permet un usage de CodeSniffer en ligne de commande
mais vous pouvez utiliser directement CodeSniffer dans votre IDE. |br|
*Possible sur Atom, PhpStorm, Eclipse ...*

.. _Php Code Sniffer: https://github.com/squizlabs/PHP_CodeSniffer

!!!!

.. rst-class:: uppercase-text

Installation

`Documentation <https://www.drupal.org/node/1419988>`_

* Installer Coder de manière globale en local ;
* Implémenter les standards de code Drupal ;
* Noter le chemin d'installation ;
* Configurer phpcs avec le chemin d'installation de CodeSniffer ;


.. code-block:: console

  # Root of the Drupal project
  composer global require drupal/coder
  composer global require dealerdirect/phpcodesniffer-composer-installer
  composer global show -P
  // php CodeSniffer Config installed_paths set to ~/.composer/vendor/drupal/coder/coder_sniffer
  phpcs -i
  phpcs --config-set installed_paths ~/workspace/coder/coder_sniffer


!!!!

.. rst-class:: uppercase-text

Utilisation

Pour vérifier le respect de votre style de code :

.. code-block:: console

  # Root of the Drupal project
  phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml /path/to/module


Pour fixer les erreurs de style :

.. code-block:: console

  # Root of the Drupal project
  phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml /path/to/module
