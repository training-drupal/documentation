.. |br| raw:: html

  <br />

.. role:: underline


🏁 Dernier tour de piste
========================

⏱️ | 40 min.

!!!!

Nous allons finir de composer notre site, en nous intéressant plus particulièrement
aux menus, blocs et formulaires.


:underline:`Les menus :`

  * Créer un menu spécifique aux sports, à positionner en ``Sidebar 2``, avec
    une liste de liens, renvoyant vers la présentation de chaque sport proposé ;
  * Créer un menu footer avec des liens vers des sites d'informations sportives ;

:underline:`Les blocs personnalisés`

    Nous allons créer deux blocs personnalisés pour afficher quelques informations
    générales sur l'association :

    Un bloc Contact :

      À afficher sur toutes les pages, dans la région de votre choix, avec quelques
      informations, comme :

      * Une image ;
      * L'adresse ;
      * Des informations de contact : téléphone, mail ;
      * Les horaires d'accueil ;

    Un bloc En savoir plus :

      Affiché sur les pages des activités sportives, ce bloc affichera les
      informations de contact des personnes responsables - référentes, coach... -
      du sport concerné.

:underline:`Les formulaires`

  Nous allons créer un formulaire pour gérer les inscriptions à la newsletter de
  l'association. Nous recueillerons les informations suivantes :

  * Nom ;
  * Mail ;
  * Code postal ;
  * Un centre d'intérêt sportif ;
  * Un consentement ;

  Deux manières pour rendre ce formulaire :

  * Niveau 1 : une page dédiée, accessible dans un menu ;
  * Niveau 2 : dans un bloc, dans la région la région de votre choix ;

  Vous installerez également un module pour sécuriser votre formulaire :
  `Honeypot <https://www.drupal.org/project/honeypot>`_ ou
  `Captcha <https://www.drupal.org/project/captcha>`_
